mortes_idade <- function(){
      if(!("readxl") %in% installed.packages()) install.packages("readxl")
      library(readxl)
      if(!("ggplot2") %in% installed.packages()) install.packages("ggplot2")
      library(ggplot2)
      if(!("janitor") %in% installed.packages()) install.packages("janitor")
      library(janitor)
      if(!("modeest") %in% installed.packages()) install.packages("modeest")
      library(modeest)
      
      dados_idade <- read_excel("dados/covid_19_bauru_mortes.xlsx")
      tabela <- tabyl(dados_idade$idade)
      print("Tabela de Frequencias:")
      print(tabela)
      idade <- as.character(tabela$`dados_idade$idade`)
      idade[length(idade)] <- "Desconhecido"
      num <- tabela$n
      #ridade <- cbind(idade)
      #rnum <- cbind(num)
      dfdados_idade <- data.frame("Idade" = idade, "Num" = num)
      print(dfdados_idade)
      
      plot <- ggplot(data = dfdados_idade, aes(x= factor (Idade, level = Idade), y=Num, fill=Idade)) + geom_bar(stat="identity", color="black", position=position_dodge())
      print(plot + labs(x = "Idade", y = "Número de Mortes") + labs(fill = "Idade"))
}